# SfM_Tools

Ce dépôt regroupe plusieurs projets de type *pipeline* pour la photogrammétrie.

## RUN_openMVG_openMVS.sh

Le lancement s'effectue dans un terminal :

```
$ bash RUN_openMVG_openMVS.sh /chemin/vers/mon/dossier/
```

* ce script requiert **openMVG** et **openMVS**
* le fichier `sensor_width_camera_database.txt` doit être présent dans le *home* utilisateur
* tous les *logs* sont horodatés et regroupés dans un seul dossier

## RUN_MVE_MVS-Texturing.sh

Le lancement s'effectue dans un terminal :

```
$ bash RUN_MVE_MVS-Texturing.sh /chemin/vers/mon/dossier/
```

* ce script requiert **MVE** et **MVS-Texturing**
* le paquet `graphviz` est nécessaire pour exécuter la commande `dot`
* tous les *logs* sont horodatés et regroupés dans un seul dossier

## Licence

Les programmes ci-joints sont publiés sous licence GPLv3.  
[www.gnu.org/licenses/gpl-3.0.fr.html](https://www.gnu.org/licenses/gpl-3.0.fr.html)
