#!/bin/bash

# Définition de la date pour chaque fichier .log
function FileName {
    DATE=`date +%Y%m%d`
    TIME=`date +%T`
    PREFIX=[${DATE}]_${TIME}
}

# Test pour vérifier si la saisie d'un nom de dossier est correcte
if [ $# -eq 0 ]
then
    echo "Veuillez préciser le nom du dossier à traiter..."
    exit 0
fi

# Test pour vérifier si le dossier est correct
if [ ! -d $1 ]
then
    echo "Veuillez indiquer un dossier valide..."
    exit 0
fi

# Création du dossier 'MVE-MVS'
if [ ! -d $1/MVE-MVS ]
then
    mkdir $1/MVE-MVS
fi

# Création du dossier 'MVE-MVS/logs'
if [ ! -d $1/MVE-MVS/logs ]
then
    mkdir $1/MVE-MVS/logs
fi

# On se place dans le répertoire MVE-MVS
cd $1/MVE-MVS/

# Step_01 : MVE/makescene
FileName
makescene --images-only $1 ./ |& tee "logs/$PREFIX"_01of10_makescene.log

# Step_02 : MVE/sfmrecon
FileName
sfmrecon ./ |& tee "logs/$PREFIX"_02of10_sfmrecon.log

# Step_03 : MVE/prebundle
FileName
prebundle --graph-mode=prebundle.dot prebundle.sfm |& tee "logs/$PREFIX"_03of10_prebundle.log
dot -Tsvg prebundle.dot -o prebundle-graph.svg

# Step_04 : MVE/bundle2pset
FileName
bundle2pset synth_0.out synth_0.ply |& tee "logs/$PREFIX"_04of10_bundle2pset.log

# Step_05 : MVE/dmrecon
FileName
dmrecon -s2 ./ |& tee "logs/$PREFIX"_05of10_dmrecon.log

# Step_06 : MVE/scene2pset
FileName
scene2pset -F2 ./ pset-L2.ply |& tee "logs/$PREFIX"_06of10_scene2pset.log

# Step_07 : MVE/fssrecon
FileName
fssrecon pset-L2.ply surface-L2.ply |& tee "logs/$PREFIX"_07of10_fssrecon.log

# Step_08 : MVE/meshclean
FileName
meshclean -t10 surface-L2.ply surface-L2-clean.ply |& tee "logs/$PREFIX"_08of10_meshclean.log

# Step_09 : MVE/mesh2pset
FileName
mesh2pset surface-L2-clean.ply pset-L2-clean.ply |& tee "logs/$PREFIX"_09of10_mesh2pset.log

# Step_10 : MVS-Texturing/texrecon
FileName
texrecon ./::undistorted surface-L2-clean.ply surface-L2-clean-textured |& tee "logs/$PREFIX"_10of10_texrecon.log
