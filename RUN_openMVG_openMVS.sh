#!/bin/bash

# Définition de la date pour chaque fichier .log
function FileName {
    DATE=`date +%Y%m%d`
    TIME=`date +%T`
    PREFIX=[${DATE}]_${TIME}
}

# Test pour vérifier si la saisie d'un nom de dossier est correcte
if [ $# -eq 0 ]
then
    echo "Veuillez préciser le nom du dossier à traiter..."
    exit 0
fi

# Test pour vérifier si le dossier est correct
if [ ! -d $1 ]
then
    echo "Veuillez indiquer un dossier valide..."
    exit 0
fi

# Création du dossier 'MVG-MVS'
if [ ! -d $1/MVG-MVS ]
then
    mkdir $1/MVG-MVS
else
    echo $'\n'"** Attention : le répertoire de destination existe déjà."
    read -p "-> Pressez [Enter] pour continuer, ou [Ctrl-C] pour quitter."$'\n'
fi

# Création du dossier 'MVG-MVS/logs'
if [ ! -d $1/MVG-MVS/logs ]
then
    mkdir $1/MVG-MVS/logs
fi

# On se place dans le répertoire MVG-MVS
cd $1/MVG-MVS/

# Step_01 : openMVG/openMVG_main_SfMInit_ImageListing
FileName
openMVG_main_SfMInit_ImageListing -i $1 -d ~/sensor_width_camera_database.txt -o matches/ |& tee "logs/$PREFIX"_01of10_SfMInit_ImageListing.log

# Step_02 : openMVG/openMVG_main_ComputeFeatures
FileName
openMVG_main_ComputeFeatures -i matches/sfm_data.json -o matches/ -n $(grep -c processor /proc/cpuinfo) |& tee "logs/$PREFIX"_02of10_ComputeFeatures.log

# Step_03 : openMVG/openMVG_main_ComputeMatches
FileName
openMVG_main_ComputeMatches -i matches/sfm_data.json -o matches/ |& tee "logs/$PREFIX"_03of10_ComputeMatches.log

# Step_04 : openMVG/openMVG_main_IncrementalSfM
FileName
openMVG_main_IncrementalSfM -i matches/sfm_data.json -m matches/ -o reconstruction_sequential/ |& tee "logs/$PREFIX"_04of10_IncrementalSfM.log

# Step_05 : openMVG/openMVG_main_ComputeSfM_DataColor
FileName
openMVG_main_ComputeSfM_DataColor -i reconstruction_sequential/sfm_data.bin -o reconstruction_sequential/sfm_data_colorized.ply |& tee "logs/$PREFIX"_05of10_ComputeSfM_DataColor.log

# Step_06 : openMVG/openMVG_main_openMVG2openMVS
FileName
openMVG_main_openMVG2openMVS -i reconstruction_sequential/sfm_data.bin -d undistorted_images/ -o scene.mvs |& tee "logs/$PREFIX"_06of10_openMVG2openMVS.log

# Step_07 : openMVS/DensifyPointCloud
FileName
DensifyPointCloud --resolution-level 2 scene.mvs
mv DensifyPointCloud-*.log "logs/$PREFIX"_07of10_DensifyPointCloud.log

# Step_08 : openMVS/ReconstructMesh
FileName
ReconstructMesh scene_dense.mvs
mv ReconstructMesh-*.log "logs/$PREFIX"_08of10_ReconstructMesh.log

# Step_09 : openMVS/RefineMesh
FileName
RefineMesh --resolution-level 2 scene_dense_mesh.mvs
mv RefineMesh-*.log "logs/$PREFIX"_09of10_RefineMesh.log

# Step_10 : openMVS/TextureMesh
FileName
TextureMesh scene_dense_mesh_refine.mvs
mv TextureMesh-*.log "logs/$PREFIX"_10of10_TextureMesh.log
